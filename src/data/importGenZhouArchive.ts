import { map, filter, mergeMap, from, bindNodeCallback } from "rxjs"
import * as R from "ramda"
import * as PD from "./postData"
import * as Dates from "date-fns"
import * as fflate from "fflate"
import * as localforage from "localforage"

export function genZhouArchive(url: string) {
  return from(loadArchive(url)).pipe(
    mergeMap(R.nAry(1, bindNodeCallback((data: ArrayBuffer, cb: (e: fflate.FlateError|null, s: Uint8Array) => void) => fflate.decompress(new Uint8Array(data), {}, cb)))),
    map(s => fflate.strFromU8(s)),
    mergeMap(txt => from(txt.split("\n"))),
    map(t => R.tryCatch(JSON.parse, R.always(null))(t)),
    filter(R.complement(R.isNil)),
    map(restructurePostObject)
  )
}

async function loadArchive(url: string) {
  const KEY = "genZhouArchive.json.gz@1"
  const stored = await localforage.getItem<ArrayBuffer>(KEY)
  if (!stored || stored.byteLength != 9195676) {
    const request = await fetch(url)
    const buffer  = await request.arrayBuffer()
    try {
      return await localforage.setItem(KEY, buffer)
    } catch (e) {
      console.warn("Could not save the downloaded archive.", e)
      return buffer
    }
  }
  console.log("Cached archive found")
  return stored
}

function extractContent(rawPost: any, commentCount: number): PD.PostContent {
  const isText = rawPost.is_self
  return {
    title: unescapeHtmlEntities(rawPost.title),
    type : isText ? "text" : "url", // image?
    data : isText ? unescapeHtmlEntities(rawPost.selftext) : rawPost.url,
    flair: rawPost.link_flair_text,
    commentCount
  }
}

const textarea = document.createElement("textarea")

function unescapeHtmlEntities(text: string) {
  textarea.innerHTML = text
  return textarea.value
}

function extractComment(obj: any): [number, PD.Comment] {
  const { data } = obj
  const [ childCount, children ] = extractComments(data.replies?.data?.children)
  const comment: PD.Comment = {
    author: data.author,
    reddit_id: data.id,
    timestamp: Dates.fromUnixTime(data.created),
    score: data.score,
    content: unescapeHtmlEntities(data.body),
    children
  }
  return [ 1 + childCount, comment ]
}

function extractComments(data?: any[]): [number, PD.Comment[]] {
  if (!data) {
    return [ 0, [] ]
  }
  let count = 0
  let result = []
  for (const obj of data) {
    if (obj.kind != "more") { // looks like some comments have been lost
      const [ myCount, comment ] = extractComment(obj)
      count += myCount
      result.push(comment)
    }
  }
  return [ count, result ]
}

function restructurePostObject(entry: any): PD.Post {
  const rawPost: any = entry.json[0].data.children[0].data
  const [ commentCount, children ] =
    extractComments(entry.json[1].data.children)
  const content: PD.PostContent = extractContent(rawPost, commentCount)

  return {
    reddit_id: entry.id,
    author: rawPost.author,
    timestamp: Dates.parseISO(entry.timestamp.$date),
    content,
    children,
    score: rawPost.score,
  }
}