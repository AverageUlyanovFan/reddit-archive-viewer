export interface Entry<Content, Child> {
  author: string
  reddit_id: string
  timestamp: Date
  content: Content
  children: Child[]

  score: number
}

export type Comment = Entry<string, Comment>
export type PostContent = {
  title: string
  type: 'url' | 'text'
  data: string
  flair?: string
  commentCount: number
}
export type Post = Entry<PostContent, Comment>