import React from 'react';
import './App.css';
import 'antd/dist/antd.css'
import { Layout, Menu, Breadcrumb, Typography, Divider, Input } from 'antd';
import { Post } from './data/postData';
import { Outlet, useNavigate, useSearchParams } from 'react-router-dom';
import { Link as RLink } from "react-router-dom"
const { Link } = Typography

const { Header, Content, Footer } = Layout;

function App() {
  const [search] = useSearchParams()
  const navigate = useNavigate()
  return (
    <Layout className="layout">
    <Header>
      <div className="logo" />
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['GenZhou']}>
        <Menu.Item key="GenZhou"><RLink to="/"><b>r/GenZhou archive</b></RLink></Menu.Item>
        <div className="search-container"><Input.Search placeholder="Search..." allowClear onSearch={(term) => {
          const newSearch = new URLSearchParams(search)
          if (term) {
            newSearch.set("filter", term)
          } else {
            newSearch.delete("filter")
          }
          navigate(`/?${newSearch}`)
        }}/></div>
      </Menu>
    </Header>
    <Content style={{ padding: '0 50px'}}>
      <div className="site-layout-content">
        <Outlet/>
      </div>
    </Content>
    <Footer style={{ textAlign: 'center' }}>
      <Link href="https://lemmygrad.ml/c/GenZhouArchive">c/GenZhouArchive</Link>
      <Divider type="vertical"/>
      <Link href="https://gitlab.com/AverageUlyanovFan/reddit-archive-viewer">Source code</Link>
    </Footer>
  </Layout>
  );
}

export default App;
