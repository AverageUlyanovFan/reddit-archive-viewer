import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import { genZhouArchive } from "./data/importGenZhouArchive"
import { Post } from './data/postData';
import { Routes, Route, HashRouter } from 'react-router-dom';
import PostsList from './ui/PostsList';
import PostAndComments from './ui/PostAndComments';

const container = document.getElementById('root')!
const root = createRoot(container)

function bootstrapStorage(): Promise<Post[]> {
  return new Promise((resolve) => {
    console.log("Loading posts...")
    const posts: Post[] = []
    genZhouArchive("/GenZhouArchive.json.gz")
      .subscribe(data => posts.push(data))
      .add(() => {
        posts.sort((lhs, rhs) => lhs.timestamp.valueOf() - rhs.timestamp.valueOf())
        resolve(posts)
      })
  })
}

const postsPromise = bootstrapStorage()

function Root() {
  const [posts, setPosts] = React.useState<Post[]>()
  React.useEffect(() => {
    postsPromise.then(data => {
      console.log(`${data.length} posts loaded`)
      setPosts(data)
    })
  }, [])

  return (
    <React.StrictMode>
      <HashRouter>
        <Routes>
          <Route path="/" element={<App />}>
            <Route index element={<PostsList posts={posts}/>}/>
            <Route path="/page/:pageNumber" element={<PostsList posts={posts}/>}/>
            <Route path="/posts/:postId" element={<PostAndComments posts={posts}/>}/>
          </Route>
        </Routes>
      </HashRouter>
    </React.StrictMode>
  )
}

root.render(<Root/>);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
