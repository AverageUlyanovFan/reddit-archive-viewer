import React from 'react'
import * as Antd from "antd"
import * as R from "ramda"
import {ArrowUpOutlined, ArrowDownOutlined, MessageOutlined} from "@ant-design/icons"
import { Post } from '../data/postData'
import ReactMarkdown from "react-markdown"
import { formatRelative } from "date-fns"
import { Link, useSearchParams, useParams, useNavigate } from "react-router-dom"

const DEFAULT_PAGE_SIZE = 10

export default function PostsList({posts}: {posts?: Post[]}) {
  const [ searchParams, setSearchParams ] = useSearchParams()
  const { pageNumber } = useParams()
  const current = parseInt(pageNumber || "1", 10)
  const navigate = useNavigate()
  const pageSize = parseInt(searchParams.get("pageSize") || "", 10) || DEFAULT_PAGE_SIZE
  const search = searchParams.get("filter")?.toLowerCase();
  const dataSource = React.useMemo(() => 
    (!search || !posts)
      ? posts
      : posts.filter(p => p.content.title.toLowerCase().includes(search)),
  [ search, posts ])
  if (typeof posts == 'undefined') {
    return <>
      {R.repeat(0, 20).map((_, i) => <Antd.Skeleton key={i} active paragraph={{rows: 2}}/>)}
    </>
  }

  return (
    <Antd.List
      itemLayout="vertical"
      size="large"
      dataSource={dataSource}
      renderItem={itemRenderer}
      pagination={{
        position: 'both',
        current,
        onChange(page) {
          if (page != current) {
            const params = searchParams.toString()
            navigate(page > 1 ? `/page/${page}?${params}` : `/?${params}`)
          }
        },
        onShowSizeChange(old, next) {
          const updated = new URLSearchParams(searchParams)
          if (next != DEFAULT_PAGE_SIZE) {
            updated.set("pageSize", next.toString())
          } else {
            updated.delete("pageSize")
          }
          setSearchParams(updated, { replace: true })
        },
        pageSize,
      }}
    />
  )
}

const flairColors: { [k: string]: string } = {
  "Question": "gray",
  "Video": "red",
  "Podcast": "red",
  "Other": "darkgreen",
  "Article": "volcano",
  "Analysis": "purple",
}
function FlairBadge(props: {text?: string, children: JSX.Element}) {
  const {text, children} = props
  if (!text) {
    return children
  }
  const color = flairColors[text]
  return <Antd.Badge.Ribbon text={text} color={color}>{children}</Antd.Badge.Ribbon>
}

function itemRenderer(post: Post) {
  const Item = Antd.List.Item
  const isText = post.content.type === "text"
  const title = isText
    ? <Link to={`/posts/${post.reddit_id}`}>{post.content.title}</Link>
    : <Antd.Typography.Link href={post.content.data}>{post.content.title}</Antd.Typography.Link>
  return (
    <FlairBadge text={post.content.flair}>
      <Item
        key={post.reddit_id}
        actions={[
          <IconText icon={ArrowUpOutlined} text={post.score}/>,
          <Link to={`/posts/${post.reddit_id}`}><IconText icon={MessageOutlined} text={post.content.commentCount.toString()}/></Link>,
        ]}
      >
        <Item.Meta title={title} description={`by ${post.author} | ${formatRelative(post.timestamp, new Date())}`}/>
        {isText ? <ReactMarkdown>{post.content.data}</ReactMarkdown> : null}
      </Item>
    </FlairBadge>
  )
}

function IconText({ icon, text }: {icon: any, text: any}) {
  return (
    <Antd.Space>
      {React.createElement(icon)}
      {text}
    </Antd.Space>
  )
};