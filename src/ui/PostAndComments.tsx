import { Comment, Post } from "../data/postData";
import React from "react"
import { useParams } from "react-router";
import * as R from "ramda"
import * as Antd from "antd"
import ReactMarkdown from "react-markdown"
import { LinkOutlined, ArrowUpOutlined } from "@ant-design/icons"
import { format } from "date-fns"
const { Title, Link, Text } = Antd.Typography

export default function PostAndComments({posts}: {posts?: Post[]}) {
  const postId: string = (useParams() as any).postId
  if (!posts) {
    return <Antd.Spin/>
  }
  const post = posts && R.find(p => p.reddit_id === postId, posts)
  if (!post) {
    return <span>404</span>
  }
  return (
    <article>
      <Title level={2}>{post.content.title}</Title>
      <Text>by {post.author}</Text>
      <Antd.Divider type="vertical"></Antd.Divider>
      <Text>{format(post.timestamp, "PPPPpp")}</Text>
      <Antd.Divider/>
      {
        post.content.type == "text"
         ? <ReactMarkdown>{post.content.data}</ReactMarkdown>
         : <Link href={post.content.data} copyable target="_blank">
             <LinkOutlined/> {post.content.data}
           </Link>
      }
      <Antd.Divider/>
      <RedditComments>{post.children}</RedditComments>
    </article>
  )
}

function RedditComments({ children }: { children: Comment[] }) {
  return <>
    {children.map((c, i) => <RedditComment key={i} comment={c}/>)}
  </>
}

function RedditComment({ comment }: { comment: Comment }) {
  return (
    <Antd.Comment
      actions={[]}
      author={`${comment.author} @ ${format(comment.timestamp, "PPpp")}`}
      avatar={<><ArrowUpOutlined/> {comment.score}</>}
      content={<ReactMarkdown>{comment.content}</ReactMarkdown>}
    >
      <RedditComments>{comment.children}</RedditComments>
    </Antd.Comment>
  );
}